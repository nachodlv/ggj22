# Heredo del nodo donde esta atacheado el script
extends KinematicBody

class_name CharacterController

# Export hace aparecer la variable en el inspector del script
export var Speed = 500.0
export var rotationSpeed = 500.0

var velocity = Vector3(0,0,0)
var isActive = false
var currentState = playerState.idle
var playerStatesNames = ["IDLE", "RUN"]
var stateMachinePlayback : AnimationNodeStateMachinePlayback

enum playerState {
	idle = 0,
	run = 1,
}

func _ready():
	stateMachinePlayback = $PlayerModel/AnimationTree.get("parameters/CharacterStateMachine/playback")
	stateMachinePlayback.start("IDLE")
	$Picker.connect("object_picked_or_released", self, "on_object_picked")

# PlayerCharacter group implementation
func set_active_character(activeCharacter : Node):
	isActive = (self == activeCharacter)

func set_state(newState):
	if currentState == newState:
		return
	
	# set up the local state and the animation state.
	currentState = newState
	stateMachinePlayback.travel(playerStatesNames[int(currentState)])
	

func on_object_picked(is_carrying_something):
	$PlayerModel/AnimationTree["parameters/IsCarryingSomething/blend_amount"] = is_carrying_something

# Fixed tick, creo que en project settings podes definir la frecuencia.
func _physics_process(delta):
	velocity = Vector3.ZERO
	
	if isActive:
		velocity.x = int(Input.is_action_pressed("MoveForward"))
		velocity.x -= int(Input.is_action_pressed("MoveBackwards"))
		velocity.z = int(Input.is_action_pressed("MoveRight"))
		velocity.z -= int(Input.is_action_pressed("MoveLeft"))
		velocity = velocity.normalized()

	# rotacion
	if velocity != Vector3.ZERO:
		var rotation = transform.origin
		var newTransform = transform.looking_at(transform.origin + velocity, Vector3.UP)
		#transform = transform.interpolate_with(newTransform, rotationSpeed * delta)
		#look_at(translation + velocity, Vector3.UP)
		transform = newTransform

	# traslacion
	velocity = move_and_slide(velocity * Speed * delta, Vector3.UP)
	
	# State Machine
	if velocity == Vector3.ZERO:
		set_state(playerState.idle)
	else:
		set_state(playerState.run)
