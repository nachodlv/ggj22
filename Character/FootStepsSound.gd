extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


export var play_foosteps = false

# Called when the node enters the scene tree for the first time.
func _ready():
	Wwise.register_game_obj(self, self.get_name())
	
func _process(delta):
	if (play_foosteps):
		play_foosteps = false
		play_footstep()

func play_footstep():
	Wwise.post_event_id(AK.EVENTS.FOOTSTEPS, self)
