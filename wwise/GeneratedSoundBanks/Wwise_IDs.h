/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID BALL_DROP_FLOOR = 3756598469U;
        static const AkUniqueID CHARACTER_CHANGE = 4220098085U;
        static const AkUniqueID DEPOSIT = 894068217U;
        static const AkUniqueID END_ENVIRONMENT = 845836586U;
        static const AkUniqueID FOOTSTEPS = 2385628198U;
        static const AkUniqueID PICKUP = 3978245845U;
        static const AkUniqueID PLAY_DEFEAT_THEME = 1757431573U;
        static const AkUniqueID PLAY_ENVIRONMENT = 1641559457U;
        static const AkUniqueID PLAY_GAMEPLAY_THEME = 454642444U;
        static const AkUniqueID PLAY_HIGHSCORE = 2349590460U;
        static const AkUniqueID PLAY_MENU_THEME = 2181630209U;
        static const AkUniqueID SPAWN_ASSET = 3769258691U;
        static const AkUniqueID THROW_OBJECT = 2210913163U;
        static const AkUniqueID UI_BOTTON = 447073508U;
        static const AkUniqueID UI_RECIPE_COMPLETE = 3272921410U;
        static const AkUniqueID UI_RECIPE_POPUP = 4039492899U;
        static const AkUniqueID UI_SCROLL = 1208240901U;
        static const AkUniqueID UI_WRONG_RECIPE = 4142751606U;
    } // namespace EVENTS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID ENVIRONMENT = 1229948536U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SFX = 393239870U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
