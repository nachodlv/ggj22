extends Spatial

class_name LookAtCamera

onready var camera = get_node("/root/Spatial/Camera")

func _process(delta):
	look_at(camera.translation, Vector3.UP)
