extends Spatial

export var dark_sphere_icon_material: Texture
export var light_sphere_icon_material: Texture

export var dark_piramid_icon_material: Texture
export var light_piramid_icon_material: Texture

export var dark_square_icon_material: Texture
export var light_square_icon_material: Texture

func display_recipee(item_descriptors, items_done):
	for i in range(0, item_descriptors.size(), 1):
		var item_descriptor: PickupDescriptor = item_descriptors[i]
		var item_done: bool = items_done[i]
		var texture_rect = get_texture_rect(i)
		
		texture_rect.visible = true
		texture_rect.get_node("BlockingPanel").visible = item_done
		texture_rect.get_node("Cross").visible = item_done
		
		var texture: Texture = get_texture_from_item(item_descriptor)
		texture_rect.texture = texture

	for i in range(item_descriptors.size(), 4, 1):
		var texture_rect = get_texture_rect(i)
		texture_rect.visible = false

func get_texture_rect(index: int):
	if (index == 0):
		return $Viewport/Recipee/TextureRect
	elif (index == 1):
		return $Viewport/Recipee/TextureRect2
	elif (index == 2):
		return $Viewport/Recipee/TextureRect3
	return $Viewport/Recipee/TextureRect4
	
func get_texture_from_item(item_descriptor: PickupDescriptor)-> Texture:
	if (item_descriptor.pickup_type == Global.PickupType.DARK):
		if (item_descriptor.pickup_form == Global.PickupForm.Sphere):
			return dark_sphere_icon_material
		elif (item_descriptor.pickup_form == Global.PickupForm.Piramid):
			return dark_piramid_icon_material
		else:
			return dark_square_icon_material
	if (item_descriptor.pickup_form == Global.PickupForm.Sphere):
		return light_sphere_icon_material
	elif (item_descriptor.pickup_form == Global.PickupForm.Piramid):
		return light_piramid_icon_material
	return light_square_icon_material
