extends Node

var Score = 0
var MaxScore = 0

enum PickupType {
	DARK = 0,
	LIGHT = 1
}

enum PickupForm {
	Sphere = 0,
	Piramid = 1,
	Cube = 2
}

func AddScore(recipeLength):
	Score += recipeLength*10
	if Score > MaxScore:
		MaxScore = Score

func SaveGame():
	var save_game = File.new()
	save_game.open("user://savegame.save", File.WRITE)
	save_game.store_line(to_json(MaxScore))
	save_game.close()
	
func LoadGame():
	var save_game = File.new()
	if not save_game.file_exists("user://savegame.save"):
		return # Error! We don't have a save to load.
		
	save_game.open("user://savegame.save", File.READ)
	var node_data = parse_json(save_game.get_line())
	MaxScore = node_data
	save_game.close()

func _ready():
	LoadGame()
