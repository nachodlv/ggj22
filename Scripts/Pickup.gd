extends Node

class_name Pickup

export var DarkMaterial : Material
export var LightMaterial: Material

export var sphere_mesh: Mesh
export var pyramid_mesh: Mesh
export var cube_mesh: Mesh

var _pickup_type = Global.PickupType.LIGHT;
var _pickup_form = Global.PickupForm.Sphere;

func set_pickup_type(pickup_type):
	_pickup_type = pickup_type;
	var mesh : MeshInstance = get_node("../CollisionShape/MeshInstance")
	if (Global.PickupType.DARK == pickup_type):
		mesh.set_material_override(DarkMaterial)
	else:
		mesh.set_material_override(LightMaterial)
		
func set_pickup_form(pickup_form):
	_pickup_form = pickup_form
	var mesh : MeshInstance = get_node("../CollisionShape/MeshInstance")
	if (Global.PickupForm.Sphere == _pickup_form):
		mesh.mesh = sphere_mesh
	elif (Global.PickupForm.Piramid == _pickup_form):
		mesh.mesh = pyramid_mesh
	else:
		mesh.mesh = cube_mesh

func get_pickup_type():
	return _pickup_type

func get_pickup_form():
	return _pickup_form

func get_projectile() -> Projectile:
	return get_parent() as Projectile

func _get_mesh() -> MeshInstance:
	return get_node("../CollisionShape/MeshInstance") as MeshInstance
