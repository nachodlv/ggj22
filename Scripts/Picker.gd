extends Node

class_name Picker

signal object_picked_or_released(object_picked)

export var throw_angle: float = -70.0
export var throw_force: float = 10.0
export var pickup_cooldown: float = 0

var current_projectile : Projectile
onready var last_pickup_time: float = OS.get_ticks_msec() / 1000.0

onready var pickup_area : Area = get_node("../PlayerModel/Area")

func _ready():
	Wwise.register_game_obj(self, self.get_name())

func _process(delta):
	if (current_projectile == null):
		_grab_falling_projectiles()

func process_input(delta):
	if (Input.is_action_just_pressed("Action")):
		var now: float = OS.get_ticks_msec() / 1000.0
		if (now - last_pickup_time > pickup_cooldown):
			last_pickup_time = now
			if (current_projectile != null):
				throw_pickup()
			else:
				try_pickup()

func throw_pickup():
	current_projectile.show()
	var body : KinematicBody = get_parent();
	
	var forward: Vector3 = -body.global_transform.basis.z
	
	var left: Vector3 = -body.global_transform.basis.x;
	var throw_rad : float = deg2rad(throw_angle)
	
	var throw_velocity = forward.rotated(left, throw_rad) * throw_force
	
	current_projectile.translation = get_node("../Spatial").global_transform.origin
	current_projectile.throw(throw_velocity)
	current_projectile = null
	Wwise.post_event_id(AK.EVENTS.THROW_OBJECT, self)
	ChangePickedMesh(null)

func try_pickup():
	var bodies = pickup_area.get_overlapping_bodies()
	var nearest_projectile: Projectile
	var min_distance = -1.0
	for body in bodies:
		var new_projectile = body as Projectile
		if (new_projectile):
			var distance : float = new_projectile.translation.distance_to(get_parent().translation)
			if (min_distance < 0.0 || distance < min_distance):
				min_distance = distance
				nearest_projectile = new_projectile

	if (nearest_projectile):
		_pickup_projectile(nearest_projectile)

func _grab_falling_projectiles():
	var bodies = pickup_area.get_overlapping_bodies()
	for body in bodies:
		var projectile = body as Projectile
		if (projectile && projectile.get_velocity().length() >= 0.1):
			if (!projectile.is_going_up() && !projectile.just_spawned()):
				_pickup_projectile(projectile)

func _pickup_projectile(projectile: Projectile):
	var now: float = OS.get_ticks_msec() / 1000.0
	last_pickup_time = now
	projectile.hide()
	current_projectile = projectile
	Wwise.post_event_id(AK.EVENTS.PICKUP, self)
	ChangePickedMesh(projectile.getMeshInstance().duplicate())

func get_pickup()->Pickup:
	return current_projectile.get_node("Pickup") as Pickup if current_projectile else null

func remove_pickup():
	current_projectile = null
	ChangePickedMesh(null)
	

func ChangePickedMesh(meshInstance: MeshInstance):
	emit_signal("object_picked_or_released", meshInstance != null)
	if (meshInstance):
		get_node("../Spatial").add_child(meshInstance)
		meshInstance.translation = Vector3()
		meshInstance.visible = true
	else:
		for node in get_node("../Spatial").get_children():
			get_node("../Spatial").remove_child(node)
