extends KinematicBody

class_name Projectile

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var gravity_going_up: float = 9.8
export var gravity_going_down: float = 4.0

var _velocity : Vector3 = Vector3()

onready var spawn_time: float = OS.get_ticks_msec() / 1000.0

var landed = true

func _ready():
	Wwise.register_game_obj(self, self.get_name())
	Wwise.post_event_id(AK.EVENTS.SPAWN_ASSET, self)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var gravity : float = gravity_going_up if is_going_up() else gravity_going_down
	_velocity += Vector3.DOWN * gravity * delta
	_velocity = move_and_slide(_velocity, Vector3.UP)
	for i in range(0, get_slide_count(), 1):
		var collision = get_slide_collision(i).collider
		if (collision == get_node("../DarkFloor")):
			get_node("../PickupSpawner").projectile_moved_area($Pickup, Global.PickupType.DARK)
			if (!landed):
				landed =true
				Wwise.post_event_id(AK.EVENTS.BALL_DROP_FLOOR, self)
		elif (collision == get_node("../LightFloor")):
			get_node("../PickupSpawner").projectile_moved_area($Pickup, Global.PickupType.LIGHT)
			if (!landed):
				landed =true
				Wwise.post_event_id(AK.EVENTS.BALL_DROP_FLOOR, self)

func is_going_up()->bool:
	return _velocity.dot(Vector3.DOWN) < 0;

func get_velocity() -> Vector3:
	return _velocity

func throw(velocity : Vector3):
	_velocity = velocity
	landed = false

func hide():
	var collisionShape : CollisionShape = $CollisionShape
	collisionShape.disabled = true
	var mesh : MeshInstance = $CollisionShape/MeshInstance
	mesh.visible = false

func show():
	var collisionShape : CollisionShape = $CollisionShape
	collisionShape.disabled = false
	var mesh : MeshInstance = $CollisionShape/MeshInstance
	mesh.visible = true
	
func just_spawned()-> bool:
	return ((OS.get_ticks_msec() / 1000.0) - spawn_time) < 1.0

func getMeshInstance() -> MeshInstance:
	return $CollisionShape/MeshInstance as MeshInstance
