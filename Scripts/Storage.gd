extends Spatial

# Bug with time limit
# Implement difficulty

export var time_between_recipees : float = 3.0
export var force_recipes_quantity = 2
export (Global.PickupType) var storage_side = Global.PickupType.LIGHT

onready var timer : Timer = get_node("Timer")
onready var healthBar : ProgressBar = _get_progress_bar()

onready var last_recipee_finished_time: float = OS.get_ticks_msec() / 1000.0 + time_between_recipees
onready var recipee_script = preload("res://Scripts/Recipee.gd")

onready var world_script : WorldScript = get_node("/root/Spatial")

onready var pickup_spawner: PickupSpawner = get_node("../PickupSpawner")

onready var area : Area = $Area

onready var explosion: Particles = $Explosion

var current_recipee = []
var recipees_done = []
var recipee_finished = false

# Called when the node enters the scene tree for the first time.
func _ready():
	Global.Score = 0.0
	timer.set_one_shot(true)
	Wwise.register_game_obj(self, self.get_name())
	create_recipee()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	healthBar.value = timer.time_left
	_check_picker_nearby()
	var now : float = OS.get_ticks_msec() / 1000.0
	if (recipee_finished && now - last_recipee_finished_time > time_between_recipees):
		create_recipee()

func _on_Timer_timeout():
	if (!recipee_finished):
		get_tree().change_scene("res://Scenes/GameOverScene.tscn")
		Global.SaveGame()

func create_recipee():
	var recipee = []
	var items_per_recipee = world_script.get_quantity_for_recipe()
	for i in range(0, items_per_recipee, 1):
		var pickup_descriptor : PickupDescriptor = recipee_script.new()
		var item_types = get_possible_item_types()
		var item_forms = get_possible_item_forms()
		var item_type = item_types[randi() % item_types.size()];
		var item_form = item_forms[randi() % item_forms.size()];
		pickup_descriptor.pickup_type = item_type
		pickup_descriptor.pickup_form = item_form
		recipee.append(pickup_descriptor)

	recipee_finished = false
	recipees_done = []
	for i in range(0, recipee.size(), 1):
		recipees_done.append(false)
	current_recipee = recipee
	var time_to_finish = world_script.get_time_for_recipe()
	healthBar.max_value = time_to_finish
	timer.paused = false
	timer.start(time_to_finish)
	Wwise.post_event_id(AK.EVENTS.UI_RECIPE_POPUP, self)
	$RecipeeDisplayer.display_recipee(current_recipee, recipees_done)
	pickup_spawner.ensure_items_in_ground(current_recipee)

func get_possible_item_types():
	if (world_script.recipes_finished < force_recipes_quantity):
		return [storage_side]
	return [Global.PickupType.DARK, Global.PickupType.LIGHT]

func get_possible_item_forms():
	return world_script.get_available_item_forms()

func _check_picker_nearby():
	var bodies = area.get_overlapping_bodies()
	for body in bodies:
		var player = body as CharacterController
		if (player):
			var picker: Picker = player.get_node("Picker")
			var pickup: Pickup = picker.get_pickup()
			if (pickup && _process_pickup(pickup)):
				pickup_spawner.projectile_destroyed(pickup)
				picker.remove_pickup()

func _process_pickup(pickup: Pickup)->bool:
	recipee_finished = true
	var pickup_found = false
	for i in range(0, current_recipee.size(), 1):
		if (!recipees_done[i]) :
			var item: PickupDescriptor = current_recipee[i]
			if (!pickup_found 
				&& item.pickup_type == pickup.get_pickup_type()
				&& item.pickup_form == pickup.get_pickup_form()):
				recipees_done[i] = true
				pickup_found = true
			else:
				recipee_finished = false
	
	$RecipeeDisplayer.display_recipee(current_recipee, recipees_done)
	if (!recipee_finished && pickup_found):
		Wwise.post_event_id(AK.EVENTS.DEPOSIT, self)
	if (recipee_finished && pickup_found):
		last_recipee_finished_time = OS.get_ticks_msec() / 1000.0
		world_script.recipes_finished += 1
		Global.AddScore(current_recipee.size())
		explosion.emitting = true
		timer.paused = true
		Wwise.post_event_id(AK.EVENTS.UI_RECIPE_COMPLETE, self)
	return pickup_found

func _get_progress_bar() -> ProgressBar:
	return (get_node("/root/Spatial/CanvasLayer/LightHealthBar/ProgressBar") if storage_side == Global.PickupType.LIGHT
		else get_node("/root/Spatial/CanvasLayer/DarkHealthBar/ProgressBar")) as ProgressBar




