extends Area

onready var pickup_spawner = get_node("../PickupSpawner")

func _ready():
	connect("body_exited", self, "something_exited")

func something_exited(body: Node):
	var projectile = body as Projectile
	if (projectile != null):
		pickup_spawner.projectile_destroyed(projectile.get_node("Pickup"))
