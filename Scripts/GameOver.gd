extends Node

onready var score_label : Label = get_node("Background/ScoreLabel")
onready var high_score_label : Label = get_node("Background/HighScoreLabel")

func _ready():
	Wwise.register_game_obj(self, self.get_name())
	

func _on_RestartButton_button_down():
	get_tree().change_scene("res://Scenes/Test.tscn")
	pass # Replace with function body.

func _on_Background_draw():
	if (Global.Score < Global.MaxScore):
		Wwise.post_event_id(AK.EVENTS.PLAY_DEFEAT_THEME, self)
	else:
		Wwise.post_event_id(AK.EVENTS.PLAY_HIGHSCORE, self)
		
	score_label.set_text("Your Score is: " + String(Global.Score))
	high_score_label.set_text("Your High Score is: " + String(Global.MaxScore))
	pass # Replace with function body.


func _on_RestartButton2_pressed():
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
