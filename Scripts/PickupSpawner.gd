extends Node

class_name PickupSpawner

export var max_items_per_area : int = 5
export var time_betweem_spawn : float = 0.001

var projectile_scene = preload("res://Models/Projectile.tscn")
onready var world_script : WorldScript = get_node("/root/Spatial") as WorldScript

var dart_pickups = []
var light_pickups = []

var time_last_spawn : float = 0.0

func _ready():
	time_last_spawn = OS.get_unix_time()
	randomize()

func _process(delta):
	var now : float = OS.get_unix_time()
	if (now - time_last_spawn > time_betweem_spawn):
		time_last_spawn = now
		_try_spawn_projectile()

func _try_spawn_projectile():
	var available_forms = world_script.get_available_item_forms()
	var item_form = available_forms[randi() % available_forms.size()]
	_spawn_projectile_forced(item_form)

func _spawn_projectile_forced(item_form, force = false):
	var use_dark_area : bool = (randi() % 2) && (force || dart_pickups.size() < max_items_per_area)
	var use_light_area: bool = !use_dark_area && (force || light_pickups.size() < max_items_per_area)

	if (!use_dark_area && !use_light_area):
		return

	var MinPoint := Vector3()
	var MaxPoint := Vector3()
	var new_projectile : KinematicBody = projectile_scene.instance()
	var pickup : Pickup = new_projectile.get_node("Pickup")
	pickup.set_pickup_form(item_form)
	if (use_dark_area):
		MinPoint = $DarkMinPointArea.global_transform.origin
		MaxPoint = $DarkMaxPointArea.global_transform.origin
		pickup.set_pickup_type(Global.PickupType.DARK)
		dart_pickups.append(pickup)
	else:
		MinPoint = $LightMinPointArea.global_transform.origin
		MaxPoint = $LightMaxPointArea.global_transform.origin
		pickup.set_pickup_type(Global.PickupType.LIGHT)
		light_pickups.append(pickup)
	var x : float = rand_range(MinPoint.x, MaxPoint.x)
	var z : float = rand_range(MinPoint.z, MaxPoint.z)
	get_owner().call_deferred("add_child", new_projectile)
	new_projectile.translation = (Vector3(x, self.transform.origin.y + 1.2, z))

func projectile_moved_area(pickup: Pickup, pickup_type):
	if (pickup.get_pickup_type() == pickup_type):
		return
	if (pickup.get_pickup_type() == Global.PickupType.DARK):
		dart_pickups.erase(pickup)
	else:
		light_pickups.erase(pickup)
	pickup.set_pickup_type(pickup_type)
	
func projectile_destroyed(pickup: Pickup):
	if (pickup.get_pickup_type() == Global.PickupType.DARK):
		dart_pickups.erase(pickup)
	else:
		light_pickups.erase(pickup)
		
func ensure_items_in_ground(item_descriptors):
	for item_descriptor in item_descriptors:
		var item_form = item_descriptor.pickup_form
		var has_form := false
		for item in dart_pickups:
			if (item_form == item.get_pickup_form()):
				has_form = true
				break
		if (!has_form):
			for item in light_pickups:
				if (item_form == item.get_pickup_form()):
					has_form = true
					break
		if (!has_form):
			_spawn_projectile_forced(item_form, true)
