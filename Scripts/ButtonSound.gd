extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	Wwise.register_game_obj(self, self.get_name())


func _on_RestartButton_button_down():
	Wwise.post_event_id(AK.EVENTS.UI_BOTTON, self)


func _on_Exit_pressed():
	Wwise.post_event_id(AK.EVENTS.UI_BOTTON, self)


func _on_Continue_pressed():
	Wwise.post_event_id(AK.EVENTS.UI_BOTTON, self)
