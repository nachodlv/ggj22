extends Spatial

class_name WorldScript

export var available_item_forms = {
	0: [Global.PickupForm.Sphere],
	10: [Global.PickupForm.Sphere, Global.PickupForm.Piramid],
	30: [Global.PickupForm.Sphere, Global.PickupForm.Piramid, Global.PickupForm.Cube]
}

export var time_for_recipe = {
	0: 500.0,
	1: 250.0,
	2: 100.0,
	3: 50.0,
	4: 40.0,
	5: 35.0,
	10: 30.0,
	15: 25.0
}

export var quantity_for_recipe = {
	0: 1,
	5: 2,
	20: 3,
	30: 4
}

var Characters = ["CharacterBlack", "CharacterWhite"]
var CharacterSelected = 0
var recipes_finished = 0

onready var GameCamera = get_node("Camera")

func _ready():
	Wwise.register_game_obj(self, self.get_name())
	get_tree().call_group("PlayerCharacter", "set_active_character", get_node(Characters[CharacterSelected]))

func _process(delta):
	if Input.is_action_just_pressed("ChangeCharacter"):
		if (!GameCamera.ChangingCharacter):
			CharacterSelected = (CharacterSelected+1) % Characters.size()
			get_tree().call_group("PlayerCharacter", "set_active_character", get_node(Characters[CharacterSelected]))
			GameCamera.MoveCamera()
			Wwise.post_event_id(AK.EVENTS.CHARACTER_CHANGE, self)
	
	var character = get_node(Characters[CharacterSelected])
	character.get_node("Picker").process_input(delta)

func get_time_for_recipe() -> float:
	var min_time: float = 0.0
	for key in time_for_recipe:
		if (key > recipes_finished):
			return min_time
		else:
			min_time = time_for_recipe[key]
	return min_time
	
func get_available_item_forms():
	var items = []
	for key in available_item_forms:
		if (key > recipes_finished):
			return items
		else:
			items = available_item_forms[key]
	return items

func get_quantity_for_recipe() -> int:
	var quantity: int = 0
	for key in quantity_for_recipe:
		if (key > recipes_finished):
			return quantity
		else:
			quantity = quantity_for_recipe[key]
	return quantity
