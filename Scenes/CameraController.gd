extends Spatial

var CameraPositions = ["../CameraPositionBlack", "../CameraPositionWhite"]
var ChangingCharacter = false
var t = 0.0

func _physics_process(delta) -> void:
	if(ChangingCharacter):
		t +=  0.01

		var CharacterSelected = get_node("../../Spatial").get("CharacterSelected")

		var NewCameraPosition = get_node(CameraPositions[CharacterSelected]).translation
		
		translation = translation.linear_interpolate(NewCameraPosition, t)
		
		if (NewCameraPosition - translation == Vector3.ZERO || t >= 1):
			ChangingCharacter = false
			t = 0
	

func MoveCamera():
	ChangingCharacter = true
